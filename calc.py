import signal
import os


def sumar():
    return a + b


def restar():
    return a - b


pid = os.getpid()

while True:
    try:
        print("Introduzca ctrl+c si quiere finalizar\n")
        a = float(input("Introduzca un primer número: "))
        b = float(input("Introduzca un segundo número: "))

        print("la suma de " + str(a) + " y " + str(b) + " es igual a: " + str(sumar()))
        print("la resta de " + str(a) + " y " + str(b) + " es igual a: " + str(restar()))

    except ValueError:
        print("Introduzca un número correcto")
    except KeyboardInterrupt:
        print(" Ha pulsado ctrl+c, el programa ha finalizado")
        os.kill(pid, signal.SIGTERM)
